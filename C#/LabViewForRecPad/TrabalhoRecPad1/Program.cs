﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabViewForRecPad;

namespace TrabalhoRecPad1
{
    class Program
    {


        static void Main(string[] args)
        {
            Class1 test = new Class1();
            var stopwatch = new Stopwatch();         
            

            double[,] Data = test.PredictoreMatrix();
            
            //Cov start Here:

            stopwatch.Start();
            double[,] Cov = test.MyCovCSharp(Data);//EQUATION 68
            stopwatch.Stop();
            Console.WriteLine($"Tempo passado EQUACAO 68: {stopwatch.Elapsed}");
            stopwatch.Restart();
            

            //EQ 69:
            stopwatch.Start();
            double[,] Cov2 = test.MyCovCSharp2(Data);
            stopwatch.Stop();
            Console.WriteLine($"Tempo passado: EQUACAO 69 {stopwatch.Elapsed}");
            stopwatch.Restart();

            //StreamWriter file = new StreamWriter(@"C:\Users\Paulo\Desktop\Mestrado\Reconhecimento de padrões\Trabalho 1\trabalho-1-recpad\CSharpCov1.csv");
            



            //EQ 70:
            stopwatch.Start();
            double[,] Cov3 = test.MyCovCSharp3(Data);
            stopwatch.Stop();
            Console.WriteLine($"Tempo passado: EQUACAO 70 {stopwatch.Elapsed}");
            stopwatch.Restart();




            //EQ Da recursividade:
            stopwatch.Start();
            double[,] Cov4 = test.MyCovCSharp4(Data);
            stopwatch.Stop();
            Console.WriteLine($"Tempo passado: EQUACAO Recursividade para CoV {stopwatch.Elapsed}");
            stopwatch.Restart();


            StreamWriter file = new StreamWriter(@"C:\Users\paulo\OneDrive\Área de Trabalho\Mestrado\RECPAD\Trabalho 1\trabalho-1-recpad\CSharpCov4.csv");
            for (int i = 0; i < Cov4.GetLength(1); i++)
            {
                for (int j = 0; j < Cov4.GetLength(0); j++)
                {
                    if (j + 1 != Cov4.GetLength(0))
                    {
                        string aux = Cov4[i, j].ToString();
                        aux = aux.Replace(',', '.');
                        file.Write(aux + ",");

                    }
                    else
                    {
                        string aux = Cov4[i, j].ToString();
                        aux = aux.Replace(',', '.');
                        file.Write(aux);

                    }

                }
                file.Write("\n"); // go to next line
            }
            file.Close();





            Console.ReadLine();
            Console.WriteLine(Cov[0, 0]);
            Console.WriteLine(Cov[2, 0]);


            double[] a1 = new double[] { 1,2,3,4};
            double[] a2 = new double[] { 1, 2, 3, 1 };
            double [,] Testing = test.SubtractFromMean(a1,a2);

            double[,] NotyherTesting = test.TransposeMatrix(Testing);
            double[,] UOU = test.MatrixProduct(Testing,NotyherTesting);
            
            Console.ReadLine();
        }
    }
}
