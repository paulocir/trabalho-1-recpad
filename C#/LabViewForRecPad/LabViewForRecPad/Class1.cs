﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabViewForRecPad
{
    public class Class1
    {
        public double[,] ArrayToColumnArray(double[] ar)
        {
            double[,] m = new double[ar.Length, 1];
            for(var i = 0; i < ar.Length; i++)
            {
                m[i, 0] = ar[i];
            }
            return m;
           

            
        }
        public double[,] Array1dTo2DArray(double [] Array)
        {
            int column = Array.Length;
            double[,] Transformed = new double[1, column];
            for(var i=0;i< column; i++)
            {
                Transformed[0, i] = Array[i];
            }

            //IMPLEMENTAR A TRANSFORMAÇÃO!!!
            return Transformed;
        }

        
        public double[,] RxMatrix(double[,] X)
        {
            double[,] Xt = TransposeMatrix(X);
            double[,] Mult = new double[Xt.GetLength(0), X.GetLength(1)];
            Mult = MatrixProduct(Xt, X);

            for(var i = 0; i < Mult.GetLength(0); i++)
            {
                for(var j = 0; j < Mult.GetLength(1); j++)
                {
                    Mult[i, j] /= X.GetLength(0);
                }
            }

            return Mult;

            
        }
        public double[,] MyRepMat(double[] Array,int Qtd)
        {
            double[,] Matrix = new double[Qtd, Array.Length];
            for(var i = 0; i < Matrix.GetLength(0); i++)
            {
                for(var j = 0; j < Matrix.GetLength(1); j++)
                {
                    Matrix[i, j] = Array[j];
                }
            }

            return Matrix;
        }
        public double[,] SubMatrix(double[,] M1, double[,] M2)
        {
            double[,] Result = new double[M1.GetLength(0), M1.GetLength(1)];
            for(var i = 0; i < Result.GetLength(0); i++)
            {
                for(var j = 0; j < Result.GetLength(1); j++)
                {
                    Result[i, j] = M1[i, j] - M2[i, j];
                }
            }

            return Result;
        }
        public double[,] Identidade(int size)
        {
            double[,] MI = new double[size, size];
            for(var i = 0; i < size; i++)
            {
                for(var j = 0; j < size; j++)
                {
                    if(i == j)
                    {
                        MI[i, j] = 1;
                    }
                }
            }

            return MI;
          
        }

        public double[,] RetrieveLineVector(double[,]X,int line)
        {

            double[] AArray = new double[X.GetLength(1)];
            for(var i=0;i< AArray.Length; i++)
            {
                AArray[i] = X[line, i];
            }
            return Array1dTo2DArray(AArray);
        }

        double[,] ScalarByMatrix(double val, double[,] M)
        {
            double[,] Result = new double[M.GetLength(0), M.GetLength(1)];
            for(int i=0;i< M.GetLength(0); i++)
            {
                for(int j = 0; j < M.GetLength(1); j++)
                {
                    Result[i, j] = M[i, j] * val;
                }
            }

            return Result;
        }

        double[,] SumMatrix(double[,] M1, double[,] M2)
        {
            double[,] Result = new double[M1.GetLength(0), M1.GetLength(1)];
            for(var i = 0; i < Result.GetLength(0); i++)
            {
                for(var j = 0; j < Result.GetLength(1); j++)
                {
                    Result[i, j] = M1[i, j] + M2[i, j];
                }
            }
            return Result;
        }
        
        public double[,] MyCovCSharp4(double[,] DataMatrix)
        {
            //EQUACAO 73:
            int posto = DataMatrix.GetLength(1);
            int Lines = DataMatrix.GetLength(0);           
            double[,] CovAtual = new double[posto, posto];
            double[] M1 = new double[posto];
            double[,] MediaAtual = Array1dTo2DArray(M1);

            double[] M2 = new double[posto];
            double[,] MediaAnterior = Array1dTo2DArray(M2);

            double[,] CovAnterior = Identidade(posto);
            
            for(int n = 1; n <= Lines; n++)
            {
                double[,] dado = RetrieveLineVector(DataMatrix, n-1);
                double scalar = (n - 1)/(double)n;
                
                MediaAtual = SumMatrix(ScalarByMatrix(scalar, MediaAnterior), ScalarByMatrix(1/(double)n, dado));
                double[,]s= SubMatrix(dado, MediaAtual);

                CovAtual = SumMatrix(ScalarByMatrix(scalar, CovAnterior),ScalarByMatrix(1 / (double)n,MatrixProduct(TransposeMatrix(s),s)));
                CovAnterior = CovAtual;
                MediaAnterior = MediaAtual;

            }
            return CovAtual;
        }
            public double[,] MyCovCSharp3(double[,] DataMatrix)
        {
            //EQUACAO 70:
            int posto = DataMatrix.GetLength(1);
            int Lines = DataMatrix.GetLength(0);
            double[] MeanAr = MeanArray(DataMatrix);
            double[,] M = MyRepMat(MeanAr, Lines);
            double[,] Cov = new double[posto, posto];
            double[,] Subb = SubMatrix(DataMatrix, M);
            double[,] Subbt = TransposeMatrix(Subb);

            Cov = MatrixProduct(Subbt, Subb);

            for(var i = 0; i < Cov.GetLength(0); i++)
            {
                for(var j = 0; j < Cov.GetLength(1); j++)
                {
                    Cov[i, j] /= Lines;
                }
            }



            return Cov;
        }
            public double[,] MyCovCSharp2(double[,] DataMatrix)
        {
            //EQUACAO 69:
            //INICIALMENTE RX(67):
            int posto = DataMatrix.GetLength(1);
            int Lines = DataMatrix.GetLength(0);            
            double[,] Cov = new double[posto, posto];
            double[,] MeanAr = Array1dTo2DArray(MeanArray(DataMatrix));
            double[,] MeanArT = TransposeMatrix(MeanAr);
            double[,] Rx = RxMatrix(DataMatrix);
            //Equaçao 69:
            double[,] Mult = MatrixProduct(MeanArT, MeanAr);
            for(int i = 0; i < Cov.GetLength(0); i++)
            {
                for(int j = 0; j < Cov.GetLength(1); j++)
                {
                    Cov[i, j] = Rx[i, j] - Mult[i, j];
                }
            }
            return Cov;

        }
        public double[,] MyCovCSharp(double[,] DataMatrix)
        {
            int posto = DataMatrix.GetLength(1);
            int Lines = DataMatrix.GetLength(0);
            double[,] Cov = new double[posto, posto];
            double[,] AuxCov = new double[posto, posto];
            double[] MeanAr = MeanArray(DataMatrix);
            double[] Xi = new double[posto];
            double[,] Subtract = new double[MeanAr.Length, 1];
            for(var i=0;i< Lines; i++)
            {
                for(var j=0; j <posto; j++)
                {
                    Xi[j] = DataMatrix[i, j];
                }
                Subtract = SubtractFromMean(Xi, MeanAr);
                var vish = TransposeMatrix(Subtract);
                AuxCov = MatrixProduct(Subtract, TransposeMatrix(Subtract));
                for(var z = 0; z < posto; z++)
                {
                    for(var k=0; k < posto; k++)
                    {
                        Cov[z, k] += AuxCov[z, k];
                        
                    }
                }
               

            }            
            for (var i = 0; i < posto; i++)
            {
                for(var j = 0; j < posto; j++)
                {
                    Cov[i, j] /= Lines;
                }
            }
            return Cov;
        }

        public double[,] MatrixProduct(double[,] m1, double[,] m2)
        {
            double[,] Matrix = new double[m1.GetLength(0), m2.GetLength(1)];
            double sum;
            for(var i = 0; i < Matrix.GetLength(0); i++)
            {
                for(var j = 0; j < Matrix.GetLength(1); j++)
                {
                    sum = 0;
                    for(var k = 0; k < m1.GetLength(1); k++)
                    {
                        sum += m1[i, k] * m2[k, j];
                    }
                    Matrix[i, j] = sum;
                }
            }
            return Matrix;
        }
        public double[,] TransposeMatrix(double [,] Matrix)
        {
            double[,] Transposed = new double[Matrix.GetLength(1), Matrix.GetLength(0)];
            for(var i=0;i< Matrix.GetLength(0); i++)
            {
                for(var j=0; j < Matrix.GetLength(1); j++)
                {
                    Transposed[j, i] = Matrix[i, j];
                }
            }
            return Transposed;
        }
        public double[,] SubtractFromMean(double[] Array, double[] MeanArray)
        {
            double [,]Result = new double[Array.Length,1];
            for(var i=0;i< Array.Length; i++)
            {
                Result[i,0] = Array[i] - MeanArray[i];
            }
            return Result;
        }
        public double[] MeanArray(double[,] matrix)
        {
            int line = matrix.GetLength(0);
            int col = matrix.GetLength(1);
            double[] result = new double[col];
            double sum;

            for(var i = 0; i < col; i++)
            {
                sum = 0;
                for(var j = 0; j < line; j++)
                {
                    sum += matrix[j, i];
                }
                result[i] = sum / line;
            }
               
                


            return result;
        }
        public double[,] CovarianceMatrixMethod1(double [,] matrix)
        {
            double[,] result = new double[matrix.GetLength(1), matrix.GetLength(1)];
            return result;
        }
        public string[] ClassArray()
        {
            string[,] data = DataMatrix();
            string[] Classes = new string[351];

            for(var i = 0; i < data.GetLength(0); i++)
            {
                Classes[i] = data[i, 34];
            }
            return Classes;
            
        }
        public double[,] PredictoreMatrix()
        {
            string[,] data = DataMatrix();
            double[,] DataP = new double[351, 34];

            for(var i = 0; i < data.GetLength(0); i++)
            {
                for(var j=0; j < data.GetLength(1); j++)
                {
                    if (j < data.GetLength(1) - 1)
                    {
                        
                        DataP[i, j] = Double.Parse(data[i, j], CultureInfo.InvariantCulture); ;
                    }
                }

            }

            return DataP;
        }
        public string[,] DataMatrix()
        {
           // string path = @"C:\Users\Paulo\Desktop\Mestrado\Reconhecimento de padrões\Trabalho 1\trabalho-1-recpad\ionosphere.csv";
           string path = @"C:\Users\paulo\OneDrive\Área de Trabalho\Mestrado\RECPAD\Trabalho 1\trabalho-1-recpad\ionosphere.csv";
            string[] lines = System.IO.File.ReadAllLines(path);
            string[,] Data = new string[351, 35];
            int i = 0, j = 0;
            foreach (var x in lines)
            {
                j = 0;
                foreach (var y in x.Split(','))
                {                    
                    Data[i, j] = y;
                    j++;
                }
                i++;                
            } 
            return Data;
        }
    }
}
