function cor_matrix = OtherCor(X) 
    numb1 = ones(size(X,1));
    I = eye(size(X,1));
    C = I - (1/351*numb1);    
    D = pinv(diag(std(X)));
    X = C*X*D;    
    cor_matrix = 1/(351-1)*(X'*X);
end