function cov_matrix = Cov3(X)
    K = size(X,1); 
    medio= sum(X)/K;
    M = repmat(medio,351,1);
    sub = (X-M)';
    cov_matrix = 1/K*((sub)*(sub'));
     
end