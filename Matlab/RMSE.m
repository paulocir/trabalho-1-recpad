function sol = RMSE(M1,M2)

    
    RMSE = M1 - M2;
    RMSE = RMSE.^2;    
    RMSE = mean(RMSE(:));
    sol = sqrt(RMSE);
    
end