function cov_matrix = Cov2(X)
    K = size(X,1); 
    medio= sum(X)/K;
    cov_matrix = (1/K)*(X'*X);
    cov_matrix = cov_matrix - medio'*medio;    
end