function cov_matrix = Cov4(X)
    z = zeros(size(X,2));
    mAnterior = z(1,:);
    CAnterior = eye(size(X,2));
    for n=1:size(X,1)
        mAtual = (((n-1)/n)*mAnterior)+(1/n*X(n,:));        
        sub =X(n,:)- mAtual;
        CAtual = (((n-1)/n)*CAnterior)+(1/n*(sub'*sub));
        CAnterior = CAtual;
        mAnterior = mAtual;
    end
    
    cov_matrix = CAtual;
     
end