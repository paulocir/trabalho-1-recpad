function cor_matrix = CorPaulo(X)  
    
    [K,N] = size(X);      
    covm = zeros(N);
    for n=1:K
       x = X(n,:);          
       produto = x'*x;       
       covm = covm+produto;       
    end         
    cor_matrix = covm/(K);
    
end