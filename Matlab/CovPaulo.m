function cov_matrix = CovPaulo(X)    
    [K N] = size(X);    
    medio= sum(X)/K;
    covm = zeros(N);
    for n=1:K
       x = X(n,:);       
       sub = (x-medio);
       t = sub';
       produto = t*sub;       
       covm = covm+produto;       
    end         
    cov_matrix = covm/(K);
    
end