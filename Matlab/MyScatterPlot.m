function MyScatterPlot(x,y,Xlabel,Ylabel,x1,y1,Xlabel1,Ylabel1,x2,y2,Xlabel2,Ylabel2,nameFile)    

    sz = 40;
    subplot(3,1,1); 
    scatter(x,y,sz,'MarkerEdgeColor',[.7 .1 .1],'MarkerFaceColor',[.9 .2 .2],'LineWidth',1.5)
    xlabel(Xlabel,'FontSize',12,'FontWeight','bold') ;
    ylabel(Ylabel,'FontSize',12,'FontWeight','bold') ;
    
    subplot(3,1,2); 
    scatter(x1,y1,sz,'MarkerEdgeColor',[.1  .7 .1],'MarkerFaceColor',[.2 .9 .2],'LineWidth',1.5)
    xlabel(Xlabel1,'FontSize',12,'FontWeight','bold') ;
    ylabel(Ylabel1,'FontSize',12,'FontWeight','bold') ;
    
    subplot(3,1,3); 
    scatter(x2,y2,sz,'MarkerEdgeColor',[.2 .1 .7],'MarkerFaceColor',[.4 .2 .9],'LineWidth',1.5)
    xlabel(Xlabel2,'FontSize',12,'FontWeight','bold') ;
    ylabel(Ylabel2,'FontSize',12,'FontWeight','bold') ;
    


    
    h=gcf;

    set(h,'PaperOrientation','landscape');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition', [0 0 1 1]);
    print(gcf, '-dpdf', nameFile);
    close;
    
   
    
end
