#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <time.h>
#define posto 34
#define dRows 351
#define MAX 100
#define LEN(arr) ((int) (sizeof (arr) / sizeof (arr)[0]))

void cov(float data[dRows][posto]){

    clock_t t;
    t = clock();
    float MeanArray[posto]={0};
    float X[posto];
    float VetorColunaSubtracao[posto][1];
    float VetorColunaTransposto[1][posto];

    float ProdutoMatriz[posto][posto];
    float Cov[posto][posto]={0};
    float sum;
    int i,j,k,w,z;
    //CALCULATE THE MEAN ARRAY:
    for(i=0;i<posto;i++){
        sum=0;
        for(j=0;j<dRows;j++){

            sum+=data[j][i];
        }
        MeanArray[i] = sum/dRows;
    }


    for(i=0;i<dRows;i++){
        for(j=0;j<posto;j++){
            X[j] = data[i][j];
        }

        for(k=0;k<posto;k++){
            VetorColunaSubtracao[k][0] = X[k] - MeanArray[k];
        }

        for(k=0;k<1;k++){
            for(w=0;w<posto;w++){
                VetorColunaTransposto[k][w] = VetorColunaSubtracao[w][k];
            }
        }

        for(k=0;k<posto;k++){
            for(w=0;w<posto;w++){
                sum=0;
                for(z=0;z<1;z++){
                    sum = sum+ VetorColunaSubtracao[k][z]*VetorColunaTransposto[z][w];
                }
                ProdutoMatriz[k][w] = sum;

            }
        }
        for(k=0;k<posto;k++){
            for(w=0;w<posto;w++){
                Cov[k][w] = Cov[k][w]+ProdutoMatriz[k][w];

            }
        }

    }
    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
            Cov[i][j]/=dRows;
        }
    }


    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    printf("cov() Eq 68 took %f seconds to execute \n", time_taken);
    printf("fim?");






}

void cov2(float data[dRows][posto]){
    clock_t t;
    t = clock();

    float MeanArray[1][posto]={0};
    float MeanArrayTransposed[posto][1];
    float dataT[posto][dRows];
    float MM[posto][posto];

    float ProdutoMatriz[posto][posto];
    float Cov[posto][posto];
    float sum;
    int i,j,k,w,z;
    //CALCULATE THE MEAN ARRAY:
    for(i=0;i<posto;i++){
        sum=0;
        for(j=0;j<dRows;j++){

            sum+=data[j][i];

        }

        MeanArray[0][i] = sum;
    }


    for(i=0;i<posto;i++){
        for(j=0;j<1;j++){
            MeanArray[j][i] = MeanArray[j][i]/dRows;

        }
    }
    //MEDIA TRANSPOSTA:
    for(i=0;i<posto;i++){
        for(j=0;j<1;j++){
            MeanArrayTransposed[i][j] = MeanArray[j][i];
        }
    }


    //PRODUTO : MMt
    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
            sum=0;
            for(k=0;k<1;k++){
                sum=sum+MeanArrayTransposed[i][k]*MeanArray[k][j];

            }

            MM[i][j] = sum;



        }

    }

    //TRANSPONTO MATRIZ
    for(i=0;i<posto;i++){
        for(j=0;j<dRows;j++){
            dataT[i][j] = data[j][i];
        }
    }
    //PRODUTO Xt * X:
    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
            sum=0;
            for(k=0;k<dRows;k++){
                sum=sum+dataT[i][k]*data[k][j];
            }
            ProdutoMatriz[i][j] = sum;
            ProdutoMatriz[i][j]/=dRows;
        }
    }

    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
                Cov[i][j] = ProdutoMatriz[i][j] - MM[i][j];

        }
    }

    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    printf("cov Eq 69 took %f seconds to execute \n", time_taken);
    printf("fim?");






}

void cov3(float data[dRows][posto]){
    printf("\n");
    clock_t t;
    t = clock();
    float MeanArray[posto];

    float M[dRows][posto];
    float SubMatrix[dRows][posto];
    float SubMatrixT[posto][dRows];

    float Cov[posto][posto];
    float sum;
    int i,j,k,w,z;

    //CALCULATE THE MEAN ARRAY:
    for(i=0;i<posto;i++){
        sum=0;
        for(j=0;j<dRows;j++){

            sum+=data[j][i];
        }
        MeanArray[i] = sum/dRows;


    }
    for(i=0;i<dRows;i++){
        for(j=0;j<posto;j++){
            SubMatrix[i][j] = data[i][j]- MeanArray[j];
            SubMatrixT[j][i] = SubMatrix[i][j];
        }
    }

    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
            sum=0;
            for(k=0;k<dRows;k++){
                sum=sum+SubMatrixT[i][k]*SubMatrix[k][j];
            }

            Cov[i][j]=sum;
            Cov[i][j]/=dRows;
        }
    }



    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    printf("cov Eq 70 took %f seconds to execute \n", time_taken);
    printf("fim?");
/*
    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
                printf("%f \n",Cov[i][j]);
        }
        printf("\n");
    }

*/





}

void cov4(float data[dRows][posto]){

    clock_t t;
    t = clock();
    float MeanArrayAnterior[1][posto]={0};
    float MeanArrayAtual[1][posto];
    float dado[1][posto];
    float dadoAux[1][posto];
    float M[dRows][posto];
    float SubMatrix[dRows][posto];
    float SubMatrixT[posto][dRows];
    float s[1][posto];

    float sT[posto][1];
    float CovAux[posto][posto];
    float CovAux2[posto][posto];
    float CovAtual[posto][posto]={0};
    float CovAnterior[posto][posto]={0};
    float sum;
    float scalar;
    int n,i,j,k,w,z;
    //IDENTIDADE:
    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
                if(i==j){
                    CovAnterior[i][j] = 1;
                }
        }
    }

    for(n=1;n<=dRows;n++){
        //RECEBENDO A N-�SIMA AMOSTRA:
        for(i=0;i<1;i++){
            for(j=0;j<posto;j++){
                dado[i][j] =data[n-1][j];
            }

        }
        scalar = (float)(n-1)/(float)n;
        //MEDIA:
        for(i=0;i<1;i++){
            for(j=0;j<posto;j++){
                MeanArrayAnterior[i][j]*=scalar;
                dadoAux[i][j]=dado[i][j]*((float)1/(float)n);
                MeanArrayAtual[i][j] = MeanArrayAnterior[i][j]+dadoAux[i][j];
                s[i][j] = dado[i][j] -MeanArrayAtual[i][j];
            }

        }
        //cov:



        for(i=0;i<posto;i++){
            for(j=0;j<1;j++){
                sT[i][j] = s[j][i];
            }

        }
        for(i=0;i<posto;i++){
            for(j=0;j<posto;j++){
                sum=0;
                for(k=0;k<1;k++){
                    sum+=sT[i][k]*s[k][j];
                }
                CovAux[i][j] = sum;

                CovAux[i][j] *= (float)1/(float)n;


            }

        }

        for(i=0;i<posto;i++){

            for(j=0;j<posto;j++){

                CovAux2[i][j] = CovAnterior[i][j]*scalar;
                CovAtual[i][j] = CovAux2[i][j] + CovAux[i][j];
                CovAnterior[i][j] = CovAtual[i][j];
            }

        }
        for(i=0;i<1;i++){
            for(j=0;j<posto;j++){
                MeanArrayAnterior[i][j] = MeanArrayAtual[i][j];
            }

        }

    }
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    printf("cov Eq 75 took %f seconds to execute \n", time_taken);
    printf("===============================================================fim?\n");
/*
    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
                printf("cov[%d][%d] = %f \n",i,j,CovAnterior[i][j]);
        }
        printf("\n");
    }




    */

    FILE *fp1;

    int array_rowit, array_colit;
    char buf[20];
    //fp1 = fopen("C:\\Users\\Paulo\\Desktop\\Mestrado\\Reconhecimento de padr�es\\Trabalho 1\\trabalho-1-recpad\\Cov4C.csv", "w");//create a file
    fp1 = fopen("C:\\Users\\paulo\\OneDrive\\�rea de Trabalho\\Mestrado\\RECPAD\\Trabalho 1\\trabalho-1-recpad\\Cov4C.csv", "w");//create a file
    if (fp1 == NULL)
    {
        printf("Error while opening the file.\n");
        return 0;
    }


    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
            if(j+1==posto){
                gcvt(CovAtual[i][j], 10, buf);
                fprintf(fp1,"%f",CovAtual[i][j]);
            }else{
                fprintf(fp1,"%f,",CovAtual[i][j]);
            }

        }
        fprintf(fp1,"\n");
    }


    fclose(fp1);

}


int main(void) {
    //char *path = "C:\\Users\\Paulo\\Desktop\\Mestrado\\Reconhecimento de padr�es\\Trabalho 1\\trabalho-1-recpad\\ionosphere.csv";
    char *path = "C:\\Users\\paulo\\OneDrive\\�rea de Trabalho\\Mestrado\\RECPAD\\Trabalho 1\\trabalho-1-recpad\\ionosphere.csv";
    char outcome[351];
    float predictors[351][34];
    int m =0;
    int n=0;

    FILE *fp = fopen(path, "r");

    char buf[1024];

    if (!fp) {
        printf("Can't open file\n");
        return 1;
    }
    int x=0;
    char charaux[10];
    while (fgets(buf, 1024, fp)) {
        for(int i=0;buf[i]!='\0';i++){
            int b =0;
            for(int i=0;i<10;i++){
                charaux[i] ="";
            }

            if(i==0){
                while(buf[i]!=','){
                    charaux[b] = buf[i];
                    b++;
                    i++;
                }
                i--;
                predictors[m][n] = strtod(charaux,NULL);
                n++;

            }else{
                if(buf[i]==','){
                    i++;
                    while(buf[i]!=','){
                        charaux[b] = buf[i];
                        b++;
                        i++;

                    }
                    predictors[m][n] = strtod(charaux,NULL);
                    n++;
                    if(n==34){
                        break;
                    }
                }
                i--;
            }
        }
        n=0;
        m++;
    }


    fclose(fp);


    FILE *fp2 = fopen(path, "r");

    char buf2[1024];

    if (!fp2) {
        printf("Can't open file\n");
        return 1;
    }

    x = 0;
    while (fgets(buf2, 1024, fp2)) {
        for(int i=0;buf2[i]!='\0';i++){
            if(buf2[i]=='g'||buf2[i]=='b'){
                outcome[x] = buf2[i];
                x++;
            }
        }
    }

    fclose(fp2);

    //FIM DA AQUISI��O DE DADOS.


    for(int i=0;i<351;i++){
        for(int j=0;j<34;j++){
            //printf("%.5f   ",predictors[i][j]);
        }
        //printf("\n");
    }

    //PointerTesting(&predictors[0][0]);
    printf("\n");
    printf("\n");
    for(int i=0;i<351;i++){
        //printf("%c",outcome[i]);
    }

    cov(predictors);
    cov2(predictors);
    cov3(predictors);
    cov4(predictors);


    printf("\n");
    printf("FIM SEM PONTEIRO");
    printf("\n");







    return 0;
}
