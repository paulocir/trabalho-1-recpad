#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <time.h>
#define posto 34
#define dRows 351

#define LEN(arr) ((int) (sizeof (arr) / sizeof (arr)[0]))



void cov2(float data[dRows][posto]){
    clock_t t;
    t = clock();

    float MeanArray[1][posto]={0};
    float MeanArrayTransposed[posto][1];
    float dataT[posto][dRows];
    float MM[posto][posto];

    float ProdutoMatriz[posto][posto];
    float Cov[posto][posto];
    float sum;
    int i,j,k,w,z;
    //CALCULATE THE MEAN ARRAY:
    for(i=0;i<posto;i++){
        sum=0;
        for(j=0;j<dRows;j++){

            sum+=data[j][i];

        }

        MeanArray[0][i] = sum;





    }


    for(i=0;i<posto;i++){
        for(j=0;j<1;j++){
            MeanArray[j][i] = MeanArray[j][i]/dRows;

        }
    }
    //MEDIA TRANSPOSTA:
    for(i=0;i<posto;i++){
        for(j=0;j<1;j++){
            MeanArrayTransposed[i][j] = MeanArray[j][i];
        }
    }


    //PRODUTO : MMt
    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
            sum=0;
            for(k=0;k<1;k++){
                sum=sum+MeanArrayTransposed[i][k]*MeanArray[k][j];

            }

            MM[i][j] = sum;



        }

    }

    //TRANSPONTO MATRIZ
    for(i=0;i<posto;i++){
        for(j=0;j<dRows;j++){
            dataT[i][j] = data[j][i];
        }
    }
    //PRODUTO Xt * X:
    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
            sum=0;
            for(k=0;k<dRows;k++){
                sum=sum+dataT[i][k]*data[k][j];
            }
            ProdutoMatriz[i][j] = sum;
            ProdutoMatriz[i][j]/=dRows;
        }
    }

    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
                Cov[i][j] = ProdutoMatriz[i][j] - MM[i][j];

        }
    }

    for(i=0;i<posto;i++){
        for(j=0;j<posto;j++){
            printf("%.4f ",Cov[i][j]);
        }
        printf("\n");
    }






    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    printf("cov Eq 69 took %f seconds to execute \n", time_taken);
    printf("fim?");

}


int main(void) {
    //char *path = "C:\\Users\\Paulo\\Desktop\\Mestrado\\Reconhecimento de padr�es\\Trabalho 1\\trabalho-1-recpad\\ionosphere.csv";
    char *path = "C:\\Users\\paulo\\OneDrive\\�rea de Trabalho\\Mestrado\\RECPAD\\Trabalho 1\\trabalho-1-recpad\\ionosphere.csv";
    char outcome[351];
    float predictors[351][34];
    int m =0;
    int n=0;

    FILE *fp = fopen(path, "r");

    char buf[1024];

    if (!fp) {
        printf("Can't open file\n");
        return 1;
    }
    int x=0;
    char charaux[10];
    while (fgets(buf, 1024, fp)) {
        for(int i=0;buf[i]!='\0';i++){
            int b =0;
            for(int i=0;i<10;i++){
                charaux[i] ="";
            }

            if(i==0){
                while(buf[i]!=','){
                    charaux[b] = buf[i];
                    b++;
                    i++;
                }
                i--;
                predictors[m][n] = strtod(charaux,NULL);
                n++;

            }else{
                if(buf[i]==','){
                    i++;
                    while(buf[i]!=','){
                        charaux[b] = buf[i];
                        b++;
                        i++;

                    }
                    predictors[m][n] = strtod(charaux,NULL);
                    n++;
                    if(n==34){
                        break;
                    }
                }
                i--;
            }
        }
        n=0;
        m++;
    }


    fclose(fp);


    FILE *fp2 = fopen(path, "r");

    char buf2[1024];

    if (!fp2) {
        printf("Can't open file\n");
        return 1;
    }

    x = 0;
    while (fgets(buf2, 1024, fp2)) {
        for(int i=0;buf2[i]!='\0';i++){
            if(buf2[i]=='g'||buf2[i]=='b'){
                outcome[x] = buf2[i];
                x++;
            }
        }
    }

    fclose(fp2);

    //FIM DA AQUISI��O DE DADOS.


    for(int i=0;i<351;i++){
        for(int j=0;j<34;j++){
            //printf("%.5f   ",predictors[i][j]);
        }
        //printf("\n");
    }

    //PointerTesting(&predictors[0][0]);
    printf("\n");
    printf("\n");
    for(int i=0;i<351;i++){
        //printf("%c",outcome[i]);
    }





    cov2(predictors);




    printf("\n");
    printf("FIM SEM PONTEIRO");
    printf("\n");







    return 0;
}
